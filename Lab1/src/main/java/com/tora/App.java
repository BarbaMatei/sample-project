package com.tora;
import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        String lines;
        Scanner scanner = new Scanner(System.in);
        lines = scanner.nextLine();
        while(!lines.equals("stop")){
            String[] elements = lines.split(" ");
            try{
                if(elements[0].equals("sqrt")){
                    Operation operation = new Operation(elements[0], Double.parseDouble(elements[1]));
                    System.out.println(operation.showResult());
                }
                else if(elements[0].equals("min") || elements[0].equals("max")){
                    Operation operation = new Operation(elements[0], Double.parseDouble(elements[1]), Double.parseDouble(elements[2]));
                    System.out.println(operation.showResult());
                }
                else{
                    Operation operation = new Operation(elements[1], Double.parseDouble(elements[0]), Double.parseDouble(elements[2]));
                    System.out.println(operation.showResult());
                }
            }
            catch (ArithmeticException exception){
                System.out.println(exception.getLocalizedMessage());
            }
            lines = scanner.nextLine();
        }
    }
}
