package com.tora;

public class Operation {
    public final Double number1;
    public final Double number2;
    public String operator;

    public Operation(String operator, double number1, double number2){
        this.operator = operator;
        this.number1 = number1;
        this.number2 = number2;
    }

    public Operation(String operator, double number1){
        this.operator = operator;
        this.number1 = number1;
        this.number2 = null;
    }

    public Double calculate(){
        Double ans = 0.0;
        getOperationError();
        switch (operator){
            case "+":
                ans = number1 + number2;
                break;
            case "-":
                ans = number1 - number2;
                break;
            case "*":
                ans = number1 * number2;
                break;
            case "/":
                ans = number1 / number2;
                break;
            case "min":
                ans = Math.min(number1, number2);
                break;
            case "max":
                ans = Math.max(number1, number2);
                break;
            case "sqrt":
                ans = Math.sqrt(number1);
                break;
            default:
                break;
        }
        return ans;
    }

    public String showResult(){
        Double ans = calculate();
        String result = "";
        if(operator.equals("sqrt")){
            result = "sqrt(" + number1.toString() + ") = " + ans.toString();
        }
        else if(operator.equals("min") || operator.equals("max")){
            result = operator + "(" + number1.toString() + ", " + number2.toString() + ") = " + ans.toString();
        }
        else{
            result = number1.toString() + " " + operator + " " + number2.toString() + " = " + ans.toString();
        }
        return result;
    }

    private void getOperationError(){
        switch (operator){
            case "+":
            case "-":
            case "*":
            case "min":
            case "max":
                if(number2 == null || number1 == null){
                    throw new ArithmeticException("Operation " + operator + " need two members");
                }
                break;
            case "/":
                if(number2 == null || number1 == null){
                    throw new ArithmeticException("Operation " + operator + " need two members");
                } else if(number2 != null && number2.equals(new Double(0))){
                    throw new ArithmeticException("Cannot divide by 0");
                }
                break;
            case "sqrt":
                if(number1.doubleValue() < 0){
                    throw new ArithmeticException("Cannot square root a negative number");
                }
                if(number1 != null  && number2 != null){
                    throw new ArithmeticException("Square root takes only one number");
                }
                break;
            default:
                throw new ArithmeticException("Invalid operator");
        }
    }
}
