package com.tora;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
/**
 * Unit test for simple App.
 */
public class TestOperation {
    private Operation operation0;

    @Before
    public void setup() {
        operation0 = new Operation("sqrt", 1);
    }

    @Test
    public void testSqrt(){
        Operation operation1 = new Operation("sqrt", 1);
        Operation operation2 = new Operation("sqrt", 4);
        Operation operation3 = new Operation("sqrt", 2, 2);
        Operation operation4 = new Operation("sqrt", -2);

        assertEquals(1.0, operation0.calculate(), 0.0);

        assertEquals(1.0, operation1.calculate(), 0.0);
        assertEquals(2.0, operation2.calculate(), 0.0);
        assertThrows(ArithmeticException.class, operation3::calculate);
        assertThrows(ArithmeticException.class, operation4::calculate);
    }

    @Test
    public void testMax(){
        Operation operation1 = new Operation("max", 0, 0);
        Operation operation2 = new Operation("max", 0, 1);
        Operation operation3 = new Operation("max", 2, 0);
        Operation operation4 = new Operation("max", -2, 5);
        Operation operation5 = new Operation("max", 6, -4);
        Operation operation6 = new Operation("max", -2);

        assertEquals(0.0, operation1.calculate(), 0.0);
        assertEquals(1.0, operation2.calculate(), 0.0);
        assertEquals(2.0, operation3.calculate(), 0.0);
        assertEquals(5.0, operation4.calculate(), 0.0);
        assertEquals(6.0, operation5.calculate(), 0.0);
        assertThrows(ArithmeticException.class, operation6::calculate);
    }

    @Test
    public void testMin(){
        Operation operation1 = new Operation("min", 0, 0);
        Operation operation2 = new Operation("min", 0, 1);
        Operation operation3 = new Operation("min", 2, 0);
        Operation operation4 = new Operation("min", -2, 5);
        Operation operation5 = new Operation("min", 6, -4);
        Operation operation6 = new Operation("min", -2);

        assertEquals(0.0, operation1.calculate(), 0.0);
        assertEquals(0.0, operation2.calculate(), 0.0);
        assertEquals(0.0, operation3.calculate(), 0.0);
        assertEquals(-2.0, operation4.calculate(), 0.0);
        assertEquals(-4.0, operation5.calculate(), 0.0);
        assertThrows(ArithmeticException.class, operation6::calculate);
    }

    @Test
    public void testAddition(){
        Operation operation1 = new Operation("+", 0, 0);
        Operation operation2 = new Operation("+", 0, 1);
        Operation operation3 = new Operation("+", 2, 0);
        Operation operation4 = new Operation("+", -2, 5);
        Operation operation5 = new Operation("+", 6, -4);
        Operation operation6 = new Operation("+", 2.5, 5.5);
        Operation operation7 = new Operation("+", -2.5, 5.5);
        Operation operation8 = new Operation("+", -2);

        assertEquals(0.0, operation1.calculate(), 0.0);
        assertEquals(1.0, operation2.calculate(), 0.0);
        assertEquals(2.0, operation3.calculate(), 0.0);
        assertEquals(3.0, operation4.calculate(), 0.0);
        assertEquals(2.0, operation5.calculate(), 0.0);
        assertEquals(8.0, operation6.calculate(), 0.0);
        assertEquals(3.0, operation7.calculate(), 0.0);
        assertThrows(ArithmeticException.class, operation8::calculate);
    }

    @Test
    public void testSubtraction(){
        Operation operation1 = new Operation("-", 0, 0);
        Operation operation2 = new Operation("-", 0, 1);
        Operation operation3 = new Operation("-", 2, 0);
        Operation operation4 = new Operation("-", -2, 5);
        Operation operation5 = new Operation("-", 6, -4);
        Operation operation6 = new Operation("-", 2.5, 5.5);
        Operation operation7 = new Operation("-", -2.5, 5.5);
        Operation operation8 = new Operation("-", -2);

        assertEquals(0.0, operation1.calculate(), 0.0);
        assertEquals(-1.0, operation2.calculate(), 0.0);
        assertEquals(2.0, operation3.calculate(), 0.0);
        assertEquals(-7.0, operation4.calculate(), 0.0);
        assertEquals(10.0, operation5.calculate(), 0.0);
        assertEquals(-3.0, operation6.calculate(), 0.0);
        assertEquals(-8.0, operation7.calculate(), 0.0);
        assertThrows(ArithmeticException.class, operation8::calculate);
    }

    @Test
    public void testMultiplication(){
        Operation operation1 = new Operation("*", 0, 0);
        Operation operation2 = new Operation("*", 0, 1);
        Operation operation3 = new Operation("*", 2, 0);
        Operation operation4 = new Operation("*", -2, 5);
        Operation operation5 = new Operation("*", 6, -4);
        Operation operation6 = new Operation("*", 2.5, 5.5);
        Operation operation7 = new Operation("*", -2.5, 5.5);
        Operation operation8 = new Operation("*", -2);

        assertEquals(0.0, operation1.calculate(), 0.0);
        assertEquals(0.0, operation2.calculate(), 0.0);
        assertEquals(0.0, operation3.calculate(), 0.0);
        assertEquals(-10.0, operation4.calculate(), 0.0);
        assertEquals(-24.0, operation5.calculate(), 0.0);
        assertEquals(13.75, operation6.calculate(), 0.0);
        assertEquals(-13.75, operation7.calculate(), 0.0);
        assertThrows(ArithmeticException.class, operation8::calculate);
    }

    @Test
    public void testDivision(){
        Operation operation1 = new Operation("/", 0, 0);
        Operation operation2 = new Operation("/", 0, 1);
        Operation operation3 = new Operation("/", 2, 0);
        Operation operation4 = new Operation("/", -2, 5);
        Operation operation5 = new Operation("/", 6, -4);
        Operation operation6 = new Operation("/", 2.5, 5);
        Operation operation7 = new Operation("/", -2.5, 5);
        Operation operation8 = new Operation("/", -2);

        assertThrows(ArithmeticException.class, operation1::calculate);
        assertEquals(0.0, operation2.calculate(), 0.0);
        assertThrows(ArithmeticException.class, operation3::calculate);
        assertEquals(-0.4, operation4.calculate(), 0.0);
        assertEquals(-1.5, operation5.calculate(), 0.0);
        assertEquals(0.5, operation6.calculate(), 0.0);
        assertEquals(-0.5, operation7.calculate(), 0.0);
        assertThrows(ArithmeticException.class, operation8::calculate);
    }
}
