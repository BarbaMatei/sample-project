package com.tora;

import java.util.concurrent.ConcurrentHashMap;

public class MyConcurrentHashMap<T> implements InMemoryRepository<T> {
    ConcurrentHashMap<T,T> chm;

    public MyConcurrentHashMap(){
        chm = new ConcurrentHashMap<>();
    }

    @Override
    public void add(T elem){
        chm.put(elem, elem);
    }

    @Override
    public boolean contains(T value){
        return chm.contains(value);
    }

    @Override
    public void remove(T elem){
        chm.remove(elem, elem);
    }

}
