package com.tora;

import java.util.HashSet;

public class MyHashSet<T> implements InMemoryRepository<T>{
    private HashSet<T> elements;

    public MyHashSet() {
        elements = new HashSet<>();
    }

    @Override
    public void add(T e) {
        elements.add(e);
    }

    @Override

    public boolean contains(T e) {
        return elements.contains(e);
    }

    @Override

    public void remove(T e) {
        elements.remove(e);
    }

}
