/*
 * Copyright (c) 2005, 2014, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

package com.tora;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MyBenchmark {
    private static final Random random = new Random();
    private static final List<Integer> elements =
            IntStream.range(0, 10000).mapToObj(i -> random.nextInt()).collect(Collectors.toList());
    private static final Collection<Integer> elementsToAdd =
            IntStream.range(0, 1000).mapToObj(i -> random.nextInt()).collect(Collectors.toList());
    // Are different orders needed?
    private static final Collection<Integer> existingElements =
            random.ints(0, 10000).limit(1000).mapToObj(elements::get).collect(Collectors.toList());


    @State(Scope.Benchmark)
    public static class ArrayListState {
        MyArrayList<Integer> repository;

        @Setup(Level.Iteration)
        public void setup() {
            this.repository = new MyArrayList<>();
            elements.forEach(o -> this.repository.add(o));
        }

        @TearDown(Level.Iteration)
        public void teardown() {
            System.gc();
        }
    }

    @State(Scope.Benchmark)
    public static class ConcurrentHashMapState {
        MyConcurrentHashMap<Integer> repository;

        public ConcurrentHashMapState() {
            this.repository = new MyConcurrentHashMap<>();
            elements.forEach(o -> this.repository.add(o));
        }
    }

    @State(Scope.Benchmark)
    public static class HashSetState {
        MyHashSet<Integer> repository;

        public HashSetState() {
            this.repository = new MyHashSet<>();
            elements.forEach(o -> this.repository.add(o));
        }
    }

    @State(Scope.Benchmark)
    public static class TreeSetState {
        MyTreeSet<Integer> repository;

        public TreeSetState() {
            this.repository = new MyTreeSet<>();
            elements.forEach(o -> this.repository.add(o));
        }
    }

    @Benchmark
    public void addArrayList(ArrayListState state) {
        for (var element : elementsToAdd)
            state.repository.add(element);
    }

    @Benchmark
    public void addConcurrentHashMap(ConcurrentHashMapState state) {
        for (var element : elementsToAdd)
            state.repository.add(element);
    }

    @Benchmark
    public void addHashSet(HashSetState state) {
        for (var element : elementsToAdd)
            state.repository.add(element);
    }

    @Benchmark
    public void addTreeSet(TreeSetState state) {
        for (var element : elementsToAdd)
            state.repository.add(element);
    }

    @Benchmark
    public void containsArrayList(ArrayListState state) {
        for (var element : existingElements)
            state.repository.contains(element);
    }

    @Benchmark
    public void containsConcurrentHashMap(ConcurrentHashMapState state) {
        for (var element : existingElements)
            state.repository.contains(element);
    }

    @Benchmark
    public void containsHashSet(HashSetState state) {
        for (var element : existingElements)
            state.repository.contains(element);
    }

    @Benchmark
    public void containsTreeSet(TreeSetState state) {
        for (var element : existingElements)
            state.repository.contains(element);
    }

    @Benchmark
    public void removeArrayList(ArrayListState state) {
        for (var element : existingElements)
            state.repository.remove(element);
    }

    @Benchmark
    public void removeConcurrentHashMap(ConcurrentHashMapState state) {
        for (var element : existingElements)
            state.repository.remove(element);
    }

    @Benchmark
    public void removeHashSet(HashSetState state) {
        for (var element : existingElements)
            state.repository.remove(element);
    }

    @Benchmark
    public void removeTreeSet(TreeSetState state) {
        for (var element : existingElements)
            state.repository.remove(element);
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(MyBenchmark.class.getSimpleName())
                .forks(1)
                .build();

        new Runner(opt).run();
    }
}
