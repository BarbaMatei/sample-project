package com.tora;

import java.util.ArrayList;

public class MyArrayList<T> implements InMemoryRepository<T> {
    private ArrayList<T> elements;

    public MyArrayList() {
        elements = new ArrayList<>();
    }

    @Override
    public void add(T e) {
        elements.add(e);
    }

    @Override
    public void remove(T e) {
        elements.remove(e);
    }

    @Override
    public boolean contains(T e) {
        return elements.contains(e);
    }

}
