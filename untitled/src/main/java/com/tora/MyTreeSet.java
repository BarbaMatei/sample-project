package com.tora;

import java.util.TreeSet;

public class MyTreeSet<T> implements InMemoryRepository<T> {
    private TreeSet<T> ts;

    public MyTreeSet() {
        ts = new TreeSet<T>();
    }

    @Override
    public void add(T elem){
        ts.add(elem);
    }

    @Override
    public boolean contains(T elem){
        return ts.contains(elem);
    }

    @Override
    public void remove(T elem){
        ts.remove(elem);
    }

}
